<?php


namespace app\services;


use app\models\Currency;

/**
 * Class CurrencyService
 * @package app\services
 */
class CurrencyService
{
    public const BATCH_MAX_COUNT = 15;

    /**
     * Truncate currencies table
     */
    public function deleteAll()
    {
        \Yii::$app->db->createCommand()->truncateTable(Currency::tableName())->execute();
    }

    /**
     * @return array
     */
    public function getCurrencies()
    {
        //Read data
        $data = $this->getContent();

        //Set date for currencies
        $date = $data->attributes()->Date ?? date('d.m.Y', time());

        //Get currencies array
        $currencies = $data->Valute;

        $batchData = [];
        $count = 0;
        $j = 0;
        foreach ($currencies as $currency) {
            $batchData[$j][] = [
                (string) $currency->Name,
                (string) $currency->CharCode,
                (float) $currency->Value / $currency->Nominal,
                (string) $date
            ];

            if(++$count == self::BATCH_MAX_COUNT) {
                $count = 0;
                $j++;
            }

        }

        return $batchData;
    }

    /**
     * @return \SimpleXMLElement
     */
    private function getContent(): \SimpleXMLElement
    {
        return new \SimpleXMLElement(file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp'));
    }

}