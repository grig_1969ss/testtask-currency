<?php
namespace app\commands;

use app\repositories\CurrencyRepository;
use app\services\CurrencyService;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Class CurrencyController
 * @package app\commands
 */
class CurrencyController extends Controller
{
    private CurrencyService $currencyService;
    private CurrencyRepository $currencyRepository;

    public function __construct($id, $module, $config = [])
    {
        $this->currencyService = new CurrencyService();
        $this->currencyRepository = new CurrencyRepository();

        parent::__construct($id, $module, $config);
    }

    /**
     * @return int
     */
    public function actionIndex()
    {
        //Truncate currencies table
        $this->currencyService->deleteAll();

        //Get data for batch insert
        $data = $this->currencyService->getCurrencies();

        //Batch insert
        foreach ($data as $part) {
            $this->currencyRepository->insertMany($part);
        }


        return ExitCode::OK;
    }
}
