<?php

/**
 * @var $this yii\web\View
 * @var $routes array
 */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Currency Api Documentation</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <table class="table table-light table-success">
                <thead>
                <tr>
                    <th>Method</th>
                    <th>URI</th>
                    <th>Parameters</th>
                    <th>Headers</th>
                    <th>Response</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($routes as $route) : ?>
                        <tr>
                            <td><?= $route['method'] ?? '-'; ?></td>
                            <td><?= $route['uri'] ?? '-'; ?></td>
                            <td><?= $route['parameters'] ?? '-'; ?></td>
                            <td><?= $route['headers'] ?? '-'; ?></td>
                            <td><?= $route['response'] ?? '-'; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
