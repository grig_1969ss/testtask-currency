<?php


namespace app\controllers;


use app\models\Currency;
use app\repositories\CurrencyRepository;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class CurrencyController
 * @package app\controllers
 */
class CurrencyController extends Controller
{
    public string $modelClass = 'app\models\Currency';

    private CurrencyRepository $currencyRepository;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        $behaviors['authenticator']['class'] = HttpBearerAuth::class;
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['index', 'view'],
            'rules' => [
                [
                    'allow' => true,
                    'verbs' => ['GET']
                ]
            ]
        ];

        return $behaviors;
    }

    /**
     * CurrencyController constructor.
     * @param $id
     * @param $module
     * @param array $config
     */
    public function __construct($id, $module, $config = [])
    {
        $this->currencyRepository = new CurrencyRepository();

        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function actionIndex()
    {
        return $this->currencyRepository->getAll();
    }

    /**
     * @param string $id
     * @return Currency
     */
    public function actionView(string $id)
    {
        return $this->currencyRepository->getByCode($id);
    }
}