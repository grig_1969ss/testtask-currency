<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\rest\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_HTML;

        return $behaviors;
    }

    /**
     * Displays documentation page.
     *
     * @return string
     */
    public function actionIndex()
    {
        $routes = [
            [
                'method' => 'POST',
                'uri' => '/user/create',
                'parameters' => 'name(string), email(string)',
                'response' => 'User access token'
            ],
            [
                'method' => 'GET',
                'uri' => '/currencies',
                'headers' => 'Authorization: Bearer (Your Token)',
                'response' => 'Currencies list'
            ],
            [
                'method' => 'GET',
                'uri' => '/currency/{id}',
                'parameters' => 'id(string)',
                'headers' => 'Authorization: Bearer (Your Token)',
                'response' => 'Currency by code'
            ],
        ];

        return $this->render('index', compact('routes'));
    }
}
