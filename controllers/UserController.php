<?php


namespace app\controllers;


use app\models\User;
use app\repositories\UserRepository;
use yii\filters\AccessControl;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class UserController
 * @package app\controllers
 */
class UserController extends Controller
{
    public string $modelClass = 'app\models\User';

    private UserRepository $userRepository;

    private string $errorMessage = 'Something went wrong';

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['create'],
            'rules' => [
                [
                    'allow' => true,
                    'verbs' => ['POST']
                ]
            ]
        ];

        return $behaviors;
    }

    /**
     * UserController constructor.
     * @param $id
     * @param $module
     * @param array $config
     */
    public function __construct($id, $module, $config = [])
    {
        $this->userRepository = new UserRepository();

        parent::__construct($id, $module, $config);
    }

    /**
     * @param $name
     * @param $email
     * @return array
     */
    public function actionCreate($name, $email)
    {
        /** @var User $response */
        $response = $this->userRepository->createUser($name, $email);
        if (isset($response->accessToken)) {
            return [
                'accessToken' => $response->accessToken
            ];
        }

        return [
            'message' => $response
        ];
    }
}