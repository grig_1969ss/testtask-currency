<?php


namespace app\repositories;

use app\models\Currency;

/**
 * Class CurrencyRepository
 * @package app\repositories
 */
class CurrencyRepository
{

    /**
     * @return array
     */
    public function getAll(): array
    {
        return Currency::find()->all();
    }

    /**
     * @param string $code
     * @return Currency
     */
    public function getByCode(string $code): ?Currency
    {
        return Currency::findOne(['code' => $code]) ?? null;
    }

    /**
     * @param array $data
     * @throws \yii\db\Exception
     */
    public function insertMany(array $data)
    {
        \Yii::$app->db->createCommand()->batchInsert(
            Currency::tableName(),
            ['name', 'code', 'rate', 'date'],
            $data
        )->execute();
    }
}