<?php


namespace app\repositories;

use app\models\User;

/**
 * Class UserRepository
 * @package app\repositories
 */
class UserRepository
{
    /**
     * @param $name
     * @param $email
     * @return User|array
     */
    public function createUser($name, $email)
    {
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->accessToken = base64_encode($email . time());//Это можно потом с behavior-ами делать

        if($user->validate()) {
            $user->save();

            return $user;
        }

        return $user->getErrors();
    }
}