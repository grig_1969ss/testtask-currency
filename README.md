Installation

1) git clone
2) composer install
3) create database currency-task(utf8_general_ci encoding)
4) yii migrate
5) create virtual host (directory -> testtask-currency/web) ext. http://currency-task/
6) go to home page for more information(http://currency-task/) 
7) insert list of currencies with command -> yii currency